#import <Foundation/Foundation.h>

@interface NSString (TTS)
- (void)speak;
@end