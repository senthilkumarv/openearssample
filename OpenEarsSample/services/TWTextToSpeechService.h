#import <Foundation/Foundation.h>
#import <OpenEars/OpenEarsEventsObserver.h>

@interface TWTextToSpeechService : NSObject <OpenEarsEventsObserverDelegate>

-(void) speak: (NSString *) textToSpeak;
+ (TWTextToSpeechService *) sharedInstance;

@property (readonly) BOOL speaking;

@end