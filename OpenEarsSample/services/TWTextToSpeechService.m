#import <OpenEars/FliteController.h>
#import <Slt/Slt.h>
#import "TWTextToSpeechService.h"

@interface TWTextToSpeechService() {
    FliteController *_fliteController;
    Slt *_slt;
    OpenEarsEventsObserver *_openEarsEventsObserver;
    NSMutableArray *_toSpeakList;
}

@property BOOL speaking;
@end

@implementation TWTextToSpeechService

+ (TWTextToSpeechService *)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] initService];
    });
    return _sharedObject;
}

- (TWTextToSpeechService *)initService {
    self = [super init];
    if(self) {
        _fliteController = [[FliteController alloc] init];
        _slt = [[Slt alloc] init];
        _openEarsEventsObserver = [[OpenEarsEventsObserver alloc] init];
        _toSpeakList = [NSMutableArray array];
        _speaking = false;
        [_openEarsEventsObserver setDelegate: self];
    }
    return self;
}

- (TWTextToSpeechService *) init {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:@"-init is not a valid initializer for the class Foo"
                                 userInfo:nil];
}

- (void) speak: (NSString *) textToSpeak {
    if(!_speaking) {
        [_fliteController say:textToSpeak withVoice: _slt];
    } else {
        [_toSpeakList addObject:textToSpeak];
    }
}

- (void)fliteDidStartSpeaking {
    [self setSpeaking:YES];
}

- (void)fliteDidFinishSpeaking {
    if([_toSpeakList count] != 0) {
        NSString *stringToSpeak = [_toSpeakList objectAtIndex:0];
        [_toSpeakList removeObjectAtIndex:0];
        [_fliteController say:stringToSpeak withVoice: _slt];
    }
    [self setSpeaking:NO];
}

@end