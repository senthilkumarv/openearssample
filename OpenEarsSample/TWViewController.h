#import <UIKit/UIKit.h>
#import <Slt/Slt.h>
#import <OpenEars/FliteController.h>

@interface TWViewController : UIViewController <OpenEarsEventsObserverDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textInput;

- (IBAction)speak:(id)sender;

@end
