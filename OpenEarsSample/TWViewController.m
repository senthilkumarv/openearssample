#import "TWViewController.h"
#import "NSString+TTS.h"

@implementation TWViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)speak:(id)sender {
    NSString *textToSpeak = [self.textInput text];
    if(![textToSpeak isEqualToString:@""]) {
        [textToSpeak speak];
    }
}

@end
