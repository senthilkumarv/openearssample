//
//  main.m
//  OpenEarsSample
//
//  Created by Senthil Kumar on 21/11/12.
//  Copyright (c) 2012 ThoughtWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TWAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TWAppDelegate class]));
    }
}
