#import "NSString+TTS.h"
#import "TWTextToSpeechService.h"


@implementation NSString (TTS)
- (void)speak {
    TWTextToSpeechService *ttsService = [TWTextToSpeechService sharedInstance];
    [ttsService speak: self];
}
@end